# Git
Git is a version-control system that locally tracks the changes in any of our files or folders on your system.

<img src="/image%20folder/git_commands.png" width="600" height="500">

### Repository 
It is file location where you are storing all the files related to your project or code.
- Local Repository  : A file location residing in your system.
- Remote Repository : It generally lies on a hosting service outside our system.

### Commit 
The commit is used to create a snapshot of the staged changes of the files as they exist at the moment. 

### Branch
The default branch name in Git is master. Different branches can be merged into any one branch as long as they belong to the same repository.

### Clone 
Clone means to get  the local copy of the code present in the repository on your local machine.

### Fork 
Fork is similar to clone but when you fork a repository, it creates a copy of original repository under your account name.

### Push
Push command is used to upload the local repository content to a remote repository.

### Merge
It is a way of putting a forked history back together again , or we could say merging different branches to one branch (usually the master branch)


